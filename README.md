## 简介

本仓库存放HCIA-HarmonyOS认证培训专用的FA应用案例代码。

## 目录结构

| 目录名 | 描述   |
| ---- | ------ | 
| smart_watering | 护花使者FA应用案例代码| 
| smart_home  | 智慧小屋FA应用案例代码  |


